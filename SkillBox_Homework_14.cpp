﻿#include <iostream>
#include <string>

int main()
{

    std::cout << "Enter your string: ";
    std::string word;
    std::getline(std::cin, word);


    std::cout << "\n" << "Your string is: " << word << "\n";
    std::cout << "Your string's length is: " << word.length() << " symbols long" << "\n";
    std::cout << "The first symbol of your string is: " << word.front() << "\n";
    std::cout << "The last symbol of your string is: " << word.back() << "\n";

    return 0;
}

